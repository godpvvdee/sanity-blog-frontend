import React from "react";
import { Element } from "react-scroll";
import { getBanner, getContact,getTabs} from "../../lib/api";
import PageWrapper from "../components/PageWrapper";
import Hero from "../sections/landing1/Hero";
import Works from "../sections/landing1/Works";
import Contact from "../sections/landing1/Contact";

const IndexPage = ({data,tabData,contactData}) => {
  console.log("done")
  return (
    <>
      <PageWrapper>
        <Hero props={data} />
        <Element name="works">
          <Works props={tabData} />
        </Element>

        <Contact props={contactData} />
      </PageWrapper>
    </>
  );
};
export default IndexPage;
export const getServerSideProps = async({ params}) =>{
  // export async function getStaticProps(context) {
    const data = await getBanner(params);
    const tabData = await getTabs(params);
    const contactData =  await getContact(params);
    return {
      props: {
        data,
        tabData,
        contactData,
      }, 
    }
  }