import client from "./client";
import imageUrlBuilder from "@sanity/image-url";

const builder = imageUrlBuilder(client);

export const urlFor = (source) => {
	return builder.image(source);
};
export const getBanner = async () => {
	return await client.fetch(
		`*[_type == "banner"] {title,description,buttonText,'image': poster.asset._ref 
	}[0]`	
		)
}
export const getTabs = async () => {
	return await client.fetch(
		`*[_type == "tab"] {tab1,tab2,tab3,tab4
		}[0]`	
		)
}
export const getContact = async () => {
	return await client.fetch(
		`*[_type == "contact"] {contactTitle,contactDescription
		}[0]`	
		)
}


